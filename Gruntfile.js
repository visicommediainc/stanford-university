module.exports = function(grunt) {

    var pkg = grunt.file.readJSON('package.json');

    var PATHS = {
        js: [
            '*.json',
            '*.js',

            'src/**/*.json',
            'src/**/*.js'
        ]
    };

    var version = pkg.version;
    var short = 	pkg.short;
    var description = 	pkg.description;
    var name = 	pkg.name;

    // Project configuration.
    grunt.initConfig({
        pkg: pkg,

        clean: [ 'dist' ],

        jshint: {
            options: {
                reporter: require('jshint-summary')
            },
            all: "!src/js/jquery*.js",
            watched: { src: [] }
        },
        jpm: {
            options: {
              src: "./dist/firefox/",
              xpi: "./public/"
            }
        },

        sync: {
            firefox: {
                files: [
                    { cwd: 'src/main/', src: [ '**', '!**/*.less', '!fonts/**/*.eot', '!fonts/**/*.svg', '!fonts/**/*.ttf' ], dest: 'dist/firefox/data/', filter: 'isFile' },
                    { cwd: 'src/firefox/', src: [ '**', '!**/*.less', '!fonts/**/*.eot', '!fonts/**/*.svg', '!fonts/**/*.ttf' ], dest: 'dist/firefox/', filter: 'isFile' },
                    { cwd: 'dist/temp/main/', src: [ '**', '!**/*.less', '!fonts/**/*.eot', '!fonts/**/*.svg', '!fonts/**/*.ttf' ], dest: 'dist/firefox/data/', filter: 'isFile' },
                ],
                ignoreInDest: '**/*.rdf',
                updateAndDelete: false
            }
        },
        'string-replace': {
            dist: {
            files: [
            {
	            expand: true,
	            cwd: 'src/shims/chrome/',
	            src: ['**/manifest.json'],
	            dest: 'temp/'+name+'/'
            },
            {
	            expand: true,
	            cwd: 'src/firefox/data/js/',
	            src: ['**/tracker.js'],
	            dest: 'dist/firefox/data/js/'
            },
             {
	            expand: true,
	            cwd: 'src/firefox/',
	            src: ['**/package.json'],
	            dest: 'dist/firefox/'
            }

            ],
            options: {
              replacements: 
              [
		            {
			            pattern: '__name__',
			            replacement: name
		            },
		            {
			            pattern: '__version__',
			            replacement: version
		            },
		            {
			            pattern: '__short__',
			            replacement: short
		            },
		            {
			            pattern: '__description__',
			            replacement: description
		            }
                ]
              }
            }
        },

    });



    // Load the plugins.
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-sync');
    grunt.loadNpmTasks('grunt-jpm');



    grunt.registerTask('setup', 'Global init', function(param) {

        var sync = grunt.config.get('sync');

        switch(param) {

            case 'firefox':
                delete sync.chrome;
                delete sync.web;

                grunt.config.set('clean', [ 'dist/temp', 'dist/firefox' ]);
                break;

        }

        grunt.config.set('sync', sync);

        return true;
    });



     grunt.registerTask('version', 'Version bubbling', function(param) {
        // This function edits the src files, which should then be committed
        var replace = require('replace');

        // Reopen config file, since it's been modified
        var version = grunt.file.readJSON('package.json').version;

        var match = version.match(/^(\d+)\.(\d+)\.(\d+)$/);
        if(!match)
            throw 'Version doesn\'t match the following format: MAJOR.MINOR.PATCH. Got "' + version + '"';

        replace({
            regex: '("version"\\s*:\\s*)"\\d+\\.\\d+\\.\\d+"',
            replacement: '$1"' + version + '"',
            paths: [
                'src/firefox/package.json'
            ],
            recursive: false,
            silent: false,
        });

        return true;
    });

    // Task definitions.
    grunt.registerTask('no-watch', ['clean', 'jshint:all', 'sync']);
    grunt.registerTask('default',  ['no-watch' , 'string-replace', 'version', 'setup:firefox', 'jpm:xpi']);
};
