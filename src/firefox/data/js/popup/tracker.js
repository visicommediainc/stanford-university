    var _gaq_version = "__version__"; 
    var _gaq_page    = window.location.pathname.replace('.html', '');
    const GA_TRACKING_ID = 'UA-1254240-8';
    
    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
    }
    
    
    var GA_CLIENT_ID = guid();
    
    function trackEvent(action, category, label) {
        try {
            let request = new XMLHttpRequest();
            let message =
              "v=1&tid=" + GA_TRACKING_ID + "&cid=" + GA_CLIENT_ID + "&aip=1" +  
              "&ds=add-on&t=event&ec=" + category + "&ea=" + action ;
             
              if (label && label != '')
                message += "&el=" + label;
            //console.log("message=" + message);
            request.open("POST", "https://www.google-analytics.com/collect", true);
            request.send(message);
        } catch (e) {
            console.log("Error sending report to Google Analytics.\n" + e);
        }
    }
  

