var MENU_ITEMS = [
	{
		name: "Stanford GSB Library",
		icon: "ico-stanfordseal.png",
		subItems: [
			{	name: "Library Home",
				uri:  "http://businesslibrary.stanford.edu"
			},
			{	name: "My Library Account",
				uri:  "http://library.stanford.edu/myaccount",
				hasSeparator: true
			},
			{	name: "Library Hours",
				uri:  "http://www.gsb.stanford.edu/library/services-facilities/hours"
			},
			{	name: "Services & Facilities",
				uri:  "http://www.gsb.stanford.edu/library/services-facilities"
			},
			{	name: "Stanford University Libraries",
				uri:  "https://library.stanford.edu"
			},

		]
	},
	{
		name: "Databases",
		icon: "ico-databases.png",
		subItems: [
			{	name: "Business Research Databases",
				uri:  "http://libguides.stanford.edu/az.php"
			},
			{	name: "Other Stanford Databases",
				uri:  "http://searchworks.stanford.edu/databases"
			},
		]
	},
	{
		name: "eJournals",
		icon: "ico-ejournal.png",
		subItems: [
			{	name: "eJournals",
				uri:  "http://sul-sfx.stanford.edu/sfxlcl41/az"
			},
			{	name: "Medical eJournals",
				uri:  "http://lane.stanford.edu/biomed-resources/ej.html"
			},
			{	name: "Google Scholar (Stanford WebLogin)",
				uri:  "http://www.gsb.stanford.edu/library/articles/ejournals/links/google.html",
				hasSeparator: true
			},
			{	name: "ABI-INFORM Global",
				uri:  "http://www.gsb.stanford.edu/library/articles/databases/links/abi"
			},
			{	name: "Business Source Complete",
				uri:  "http://www.gsb.stanford.edu/library/articles/databases/links/ebsco"
			},
			{	name: "Factiva",
				uri:  "http://www.gsb.stanford.edu/library/articles/databases/links/factiva.html"
			},
			{	name: "ScienceDirect",
				uri:  "http://www.gsb.stanford.edu/library/articles/databases/links/sciencedirect"
			},
			{	name: "JSTOR",
				uri:  "http://www.gsb.stanford.edu/library/articles/databases/links/jstor.html"
			},
			{	name: "LexisNexis Academic",
				uri:  "http://www.gsb.stanford.edu/library/articles/databases/links/Lexis_Nexis"
			},
			{	name: "APA PsycArticles Search",
				uri:  "http://www.gsb.stanford.edu/library/articles/ejournals/links/psyc"
			},
			{	name: "PsycInfo",
				uri:  "http://www.gsb.stanford.edu/library/articles/databases/links/psycinfo.html",
				hasSeparator: true
			},
			{	name: "Social Science Citation Index",
				uri:  "http://www.gsb.stanford.edu/library/articles/databases/links/social_science"
			},
			{	name: "Journal Citation Reports",
				uri:  "http://www.gsb.stanford.edu/library/articles/databases/links/jcr"
			},
		]
	},
	{
		name: "Other Resources",
		icon: "ico-other-ressources.png",
		subItems: [
			{	name: "Course Reserves",
				uri:  "http://searchworks.stanford.edu/reserves",
				hasSeparator: true
			},
			{	name: "eBooks",
				uri:  "http://www.gsb.stanford.edu/library/conduct-research/ebooks"
			},
			{	name: "Mobile Research",
				uri:  "http://libguides.stanford.edu/mobile-research"
			},
			{	name: "Selected Business Websites",
				uri:  "http://www.gsb.stanford.edu/library/conduct-research/business-websites",
				hasSeparator: true
			},
			{	name: "Faculty Publications",
				uri:  "http://www.gsb.stanford.edu/faculty-research/publications"
			},
			{	name: "Faculty Books",
				uri:  "http://www.gsb.stanford.edu/faculty-research/books"
			},
			{	name: "GSB Working Papers",
				uri:  "http://www.gsb.stanford.edu/faculty-research/working-papers"
			},
			{	name: "GSB Case Studies",
				uri:  "http://www.gsb.stanford.edu/faculty-research/case-studies",
				hasSeparator: true
			},
			{	name: "Social Science Research Network",
				uri:  "http://www.ssrn.com/"
			},
		]
	},
	{
		name: "Research Tools",
		icon: "ico-self-help-guides.png",
		subItems: [
			{	name: "Topic Guides",
				uri:  "http://www.gsb.stanford.edu/library/conduct-research/topic-guides"
			},		
			{	name: "Database Guides",
				uri:  "http://libguides.stanford.edu/database-guides",
				hasSeparator: true
			},
			{	name: "FINData",
				uri:  "http://findata.gsblibrary.org/"
			},
			{	name: "Business Research FAQ",
				uri:  "https://businessfaq.stanford.edu/"
			},
			{	name: "Remote Login Bookmarklet",
				uri:  "http://www.gsb.stanford.edu/library/help/remote-login"
			},
		]
	},
	{
		name: "Get Help",
		icon: "ico-ask-us.png",
		subItems: [
			{	name: "Ask Us (Library Help)",
				uri:  "http://www.gsb.stanford.edu/library/contact",
				hasSeparator: true
			},
			{	name: "Document Delivery Service",
				uri:  "http://jblill.stanford.edu/"
			},
			{	name: "InterLibrary Loan Service",
				uri:  "http://jblill.stanford.edu/"
			},
			{	name: "Computer Help",
				uri:  "https://helpsu.stanford.edu/helpsu/3.0/helpsu-form?portal=GSB"
			},
		]
	},
	{
		name: "Career",
		icon: "ico-career.png",
		subItems: [
			{	name: "MBA Career",
				uri:  "https://mygsb.stanford.edu/mba/career"
			},
			{	name: "MSx Career",
				uri:  "https://mygsb.stanford.edu/msx/career-management"
			},
			{	name: "My Career Dashboard",
				uri:  "https://gts.mbafocus.com/stanford/candidates/login.aspx?pid=512"
			},
			{	name: "Job Search Guide",
				uri:  "http://libguides.stanford.edu/career",
				hasSeparator: true
			},
			{	name: "Alumni Directory",
				uri:  "http://alumni.gsb.stanford.edu/directory"
			},
			{	name: "Alumni Career Services",
				uri:  "http://alumni.gsb.stanford.edu/career",
				hasSeparator: true
			},
			{	name: "ESP",
				uri:  "http://www.gsb.stanford.edu/ces/students/internships.html"
			},
			{	name: "GMIX",
				uri:  "https://mygsb.stanford.edu/mba/academics/global-experiences/global-management-immersion"
			},
			{	name: "SMIF",
				uri:  "https://mygsb.stanford.edu/mba/programs-centers/center-social-innovation/social-purpose-internships"
			},
		]
	},
	{
		name: "Other GSB Links",
		icon: "ico-login.png",
		subItems: [
			{	name: "Axess",
				uri:  "http://axess.stanford.edu/"
			},
			{	name: "Canvas",
				uri:  "http://canvas.stanford.edu"
			},
			{	name: "GSB Gmail",
				uri:  "http://webmail.stanford.edu/",
				hasSeparator: true
			},
			{	name: "MARRS",
				uri:  "https://marrs-gsb.stanford.edu/"
			},
			{	name: "MyGSB",
				uri:  "https://mygsb.stanford.edu/",
				hasSeparator: true
			},
			{	name: "Qualtrics Survey Tool",
				uri:  "https://stanfordgsb.qualtrics.com/",
				hasSeparator: true
			},
			{	name: "GSB Faculty Profiles",
				uri:  "http://www.gsb.stanford.edu/faculty-research/faculty"
			},
			{	name: "Stanford Profiles",
				uri:  "https://profiles.stanford.edu/",
				hasSeparator: true
			},
			{	name: "Stanford GSB History",
				uri:  "http://www.gsb.stanford.edu/stanford-gsb-experience/leadership/history"
			},
			{	name: "Stanford Oral History Programs",
				uri:  "https://oralhistory.stanford.edu/"
			}
		]
	}
];
