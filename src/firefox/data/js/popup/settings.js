$Settings = {

	_DEFAULTS: {
		AccessDate:  null,
		InstallDate: null,
	},

	clear: function(cb) {
		if(typeof cb === "function")
			chrome.storage.local.clear(cb);
		else
			chrome.storage.local.clear();
	},

	set: function(values, cb) {
		var validated = {};

		if(values.AccessDate === null || typeof values.AccessDate === "number")
			validated.AccessDate = values.AccessDate;

		if(values.InstallDate === null || typeof values.InstallDate === "number")
			validated.InstallDate = values.InstallDate;

		if(typeof cb === "function")
			chrome.storage.local.set(validated, $Settings.getAll.bind(null, cb));
		else
			chrome.storage.local.set(validated);
	},

	get: function(key, cb) {
		$Settings.getAll(function(settings) {
			cb(settings[key]);
		});
	},

	getAll: function(cb) {
		chrome.storage.local.get($Settings._DEFAULTS, cb);
	}

};
