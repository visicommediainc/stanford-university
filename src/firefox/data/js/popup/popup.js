    window.postMessage({message: "show"}, '*');
    window.addEventListener("init", function(event) {
        //console.log("popup.show");
        trackEvent('pageview', 'panel-show');
    });

	var setClickEvent = function(node, category, info) {
		$(node).click(function() {
		    //console.log("url:" + info.uri);
		    window.parent.postMessage({message: "navigate", url : info.uri}, '*');
		    trackEvent('click', category, info.name);
		});
	};

    var launchSearch = function() {
	    var search = $("header .box-search input").val().trim();
		if(search.length === 0)
			return;
		url = "http://searchworks.stanford.edu/?q=" + encodeURIComponent(search).replace(/%20/g, "+") + "&search_field=search";
		window.parent.postMessage({message: "navigate", url : url}, '*');
		trackEvent('search', 'search', search);
    };
    

	// Header
	setClickEvent("header #btn-logo", "Icon", { name: "Header-Library", uri: "http://www.gsb.stanford.edu/library/" });
	$("header #btn-search").click(launchSearch);

	$("header .box-search input").keydown(function(e) {
		if(e.which === 13)
			launchSearch();
	});
	
	// Footer
	setClickEvent("footer #btn-twitter",  "Icon", { name: "Footer-Twitter",  uri: "http://twitter.com/StanfordBiz" });
	setClickEvent("footer #btn-youtube",  "Icon", { name: "Footer-Youtube",  uri: "http://www.youtube.com/user/stanfordbusiness" });
	setClickEvent("footer #btn-facebook", "Icon", { name: "Footer-Facebook", uri: "http://www.facebook.com/StanfordGSB" });
	setClickEvent("footer #btn-linkedin", "Icon", { name: "Footer-LinkedIn", uri: "http://www.linkedin.com/company/stanford-graduate-school-of-business" });
	setClickEvent("footer #btn-library",  "Icon", { name: "Footer-Library",  uri: "http://www.gsb.stanford.edu/library/" });

	// Menu
	var menu = $(".menu");
	menu.css("overflow","hidden");
	
	var i, j, item, div, submenu, subitem;
	for(i = 0; i < MENU_ITEMS.length; ++i) {
		item = MENU_ITEMS[i];
		div = $("<div>" + item.name + "<i/><b/></div>")
			.addClass("menu-item")
			.attr("data-index", i)
			.css("backgroundImage", "url(../img/" + item.icon + ")");

//        outer_html = div.clone().wrap('<p>').parent().html();
//        console.log("div=" + outer_html);
		submenu = $("<ul>")
			.addClass("submenu")
			.attr("data-index", i);

		for(j = 0; j < item.subItems.length; ++j) {
			subitem = $("<li>" + item.subItems[j].name + "</li>")
				.attr("title", item.subItems[j].name)
				.addClass("submenu-item");

			//if(item.subItems[j].hasSeparator === true)
			//	subitem.addClass("separator");

			setClickEvent(subitem, item.name, item.subItems[j]);

			submenu.append(subitem);
		}

		menu.append(div);
		menu.append(submenu);
	}

	$(".menu-item").click(function() {
		var item = $(this);

		var closeOnly = item.hasClass("active");

		// Close previous submenu
		$(".menu-item.active").removeClass("active");
		$(".submenu.active").removeClass("active").slideUp('normal');

		if(closeOnly)
			return;

		// Open new submenu
		var index = item.attr("data-index");
		item.addClass("active");
		$(".submenu[data-index=" + index + "]").addClass("active").slideDown('normal');

		menu.animate({
			scrollTop: parseInt(index, 10) * 44
		});
	});
