// Extension has changed
chrome.runtime.onInstalled.addListener(function(details) {
	if(details.reason === "install")
		$Events.onInstall(details);

	else if(details.reason === "update")
		$Events.onUpdate(details);
});

// Extension's icon in Google Chrome toolbar
//chrome.browserAction.onClicked.addListener(function() {
//});

var $Events = {

	updates: [

//		// ####  Version 1.0.0.0  ####
//		{	version: "1.0.0.0",
//			run: function(funcNext) {
//				do stuff...
//
//				funcNext();
//			}
//		}
	],

	onInstall: function(details) {
		// Clear old settings
		$Settings.clear(function() {
			trackEvent("Runtime", "Install", chrome.runtime.getManifest().version);

			$Settings.set({ "InstallDate": (new Date()).getTime() });
		});
	},

	onUpdate: function(details) {
		trackEvent("Runtime", "Update", details.previousVersion + ":" + chrome.runtime.getManifest().version);

		// Check if we need to port older version's data to newer one
		var previousVersion = $Events.versionToNumber(details.previousVersion);

		var ups = $Events.updates;
		var i = -1, len = ups.length;
		function next() {
			++i;

			// Done
			if(i >= ups.length)
				return;

			if(previousVersion < $Events.versionToNumber(ups[i].version))
				ups[i].run(next);
			else
				next();
		}

		next();
	},

	versionToNumber: function(strVersion) {
		var numVersion = 0;

		var parts = strVersion.split(".");
		for(var i = 0; i < parts.length; ++i)
			numVersion = numVersion * 1000 + parseInt(parts[i]);

		return numVersion;
	},

	// So-so event for heart-beat
	onAccess: function() {
		var newAccess = function(now, what) {
			$Settings.set({ "AccessDate": now.getTime() });
			trackEvent("Runtime", "Access");
		};

		$Settings.get("AccessDate", function(accessDate) {
			var now = new Date();

			if(typeof accessDate !== "number")
				return newAccess(now);

			// Make sure `now` is after the last access date
			if(accessDate >= now.getTime())
				return;

			accessDate = new Date(accessDate);

			if(accessDate.getFullYear() !== now.getFullYear())
				return newAccess(now);

			if(accessDate.getMonth() !== now.getMonth())
				return newAccess(now);

			if(accessDate.getDate() !== now.getDate())
				return newAccess(now);
		});
	}

};



// Auto-update access
setInterval($Events.onAccess, 60000); // Each 1 minute
$Events.onAccess();
