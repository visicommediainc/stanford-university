// content-script.js
var version = "";

self.port.on("*", function(request) {
    console.log("content-script.on=" + JSON.stringify(request));
});

self.port.on('getcouponsresponse', function(request) {
    //console.log("content-script.getcouponsresponse=" + JSON.stringify(request));
    sendCustomEvent("getcouponsresponse", request);
});


function sendCustomEvent(evt , merchant) {
    var message = {"message" : merchant};
    var cloned = cloneInto(message, document.defaultView);
    var event = document.createEvent('CustomEvent');
    event.initCustomEvent(evt , true, true, cloned);
    document.documentElement.dispatchEvent(event);
    //console.log("content-script.event." + evt + " dispatched");
}

window.addEventListener('message', onMessageFromBG, false);

function onMessageFromBG (request, sender, sendResponse){
    //console.log("onMessageFromBG=" + JSON.stringify(request.data));
    if (!request.data || !request.data.message)
        return;
	switch (request.data.message) 
	{
            case "init":
                sendCustomEvent("init", {});
            break;
            case "navigate":
                self.port.emit(request.data);
            case "show" :
                version = request.version;
            break;
            default:
                console.log("Invalid request '" + request.data.message + "' from " + (sender ? sender.id : "n/a"));
            break;
    }
}

self.port.on('show', function() {  
  window.postMessage({message: "init"}, '*');
  self.port.emit("show");
});
