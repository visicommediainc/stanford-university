/*jshint moz:true*/

var prefs = require('sdk/simple-prefs').prefs;
var self  = require('sdk/self');
var { setTimeout } = require('sdk/timers');

var EH = require(self.data.url('js/event-handler'));



exports.events = {
    load   : EH(),
    unload : EH()
};



exports.exportFns = {

    main: function(options, callback) {
        var previousVersion = prefs.previousVersion || null;
        prefs.previousVersion = self.version;

        // Delay the event to make sure the background script's worker loaded properly
        setTimeout(function() {
            exports.events.load.fire({
                reason  : options.loadReason,
                version : {
                    previous : previousVersion,
                    current  : self.version
                }
            });
        }, 5000);
    },

    onUnload: function(reason) {
        exports.events.unload.fire({
            reason,
            version : {
                current: self.version
            }
        });
    }

};
