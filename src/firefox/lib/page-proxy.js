/*jshint moz:true*/

var self = require('sdk/self');



var endpoints = {};
var workers = [];



var attachWorker = function(worker) {
    // Check if already attached
    if(workers.indexOf(worker) !== -1)
        return;

    var isAlive = true;
    workers.push(worker);

    worker.on('detach', function() {
        isAlive = false;
        detachWorker(worker);
    });

    worker.port.on('page-proxy-tx', function(ev) {
        if(!ev.endpoint || !endpoints.hasOwnProperty(ev.endpoint))
            return;

        endpoints[ev.endpoint](ev.data, function(response) {
            if(!isAlive)
                return;

            ev.data = response || null;
            worker.port.emit('page-proxy-cx', ev);
        });
    });
};

var detachWorker = function(worker) {
    var index = workers.indexOf(worker);
    if(index !== -1)
        workers.splice(index, 1);
};

var registerEndpoint = function(endpoint, callback) {
    if(typeof callback === 'function')
        endpoints[endpoint] = callback;
    else
        delete endpoints[endpoint];
};

var broadcast = function(endpoint, data) {
    workers.forEach(function(worker) {
        worker.port.emit('page-proxy-cx', { id: null, endpoint, data });
    });
};

var concatPageModSettings = function(settings) {
    settings.contentScriptFile = self.data.url('content-scripts/page-proxy.js');
    settings.contentScriptWhen = 'start';
    settings.onAttach = attachWorker;

    return settings;
};



module.exports = {
    attachWorker,
    detachWorker,
    registerEndpoint,
    broadcast,
    concatPageModSettings,
};
