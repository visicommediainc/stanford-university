    var self    = require('sdk/self');
    var tabs = require("sdk/tabs");
    var proxy   = require('./page-proxy');
    var storage = require('./page-proxy-api/storage');
    var main  = require('./main');
    var utils   = require('./utils');
    var Coupon  = require('./settings').Coupon;
    var Panel   = require('./settings').Panel;
    Coupon.$api   = require('./api').api;
 
    const { PageMod } = require('sdk/page-mod');
    const ss          = require('sdk/simple-storage');
 
    tabs.on('activate', function () {
        logURL(tabs.activeTab);
        console.log('active: ' + tabs.activeTab.url);
    });
    
    tabs.on("ready", logURL);
    
    function logURL(tab) {
        console.log("tabs.16");
        main.button.state("window", {
            "label" : "panel",
            "badge" : ""
        });
        console.log("tabs.Panel.locked:" + Panel.locked);
        if (!Panel.locked)
        {
            Panel.merchantID = 0;
            Panel.merchantURL = "";
	        Panel.isPanel = 'false';
            Panel.redeem = 'false';
            main.panel.port.emit("resetcoupon", {message:"resetcoupon"});
            main.panel.hide();
            console.log("tabs.reset**");
        }
        if(tab.url.match(/^(about:|chrome:|resource:)/) || !Coupon.$settings.appEnabled)
            return;

        var url = tab.url;
        console.log("tabs.url:" + url);
        merchant = utils.getMerchantFromURL(url);
        console.log("tabs.merchant=" + JSON.stringify(merchant));
        if(merchant)
        {
			console.log("tabs.merchant found (" + merchant.MerchantID + ")");
            Coupon.$api.getMerchantCoupons(merchant.MerchantID, function(response){
                var resp = utils.parse(response); 
                console.log("tabs.getMerchantCoupons callback : " + resp.numberofresults); 
                if (resp.numberofresults > 0)
                { 
                    main.button.state("window", {
                        "label" : "alert",
                        "badge" : resp.numberofresults
                    });
                    if(Coupon.$settings.appEnabled)
                    {
                        console.log("tabs.call showalert"); 
					    Coupon.$events.showAlert(merchant, resp, url);
					    Panel.isPanel = false;
					    Panel.merchantURL = url;
                        main.button.click();
                   }
                }
            });
        }
    }

    var setGeoIP = function() {
        var url = "http://geoip.vmn.net/index.php?v=2";
        var result = utils.readAsync(url).then(function(data) {
            //console.log("geoip.data=" + data);
            var xml = utils.loadXML(data);
            if (xml)
            {
                var location = xml.getElementsByTagName("location");
                if (location)
                    Coupon.$settings.countryCode = location[0].getAttribute("country");
                console.log("tabs.countryCode=" + Coupon.$settings.countryCode);
            }
        });           
    };
    
    var init = function() {
		console.log("tabs.merchantListLastUpdate:" + Coupon.$settings.merchantListLastUpdate);
        setGeoIP();
    };

    init();
