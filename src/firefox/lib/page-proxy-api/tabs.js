/*jshint moz:true*/

var tabs = require('sdk/tabs');
var windows = require('sdk/windows').browserWindows;

var proxy = require('./../page-proxy');
var utils = require('./utils');



proxy.registerEndpoint('tabs:create', function(data, callback) {
    var { properties } = data;

    var tabOptions = {
        url: properties.url,
        onOpen: function(tab) {
            callback({
                error: false,
                tab: {
                    id: tab.id
                }
            });
        }
    };

    if('active' in properties && typeof properties.active === 'boolean')
        tabOptions.inBackground = !properties.active;

    tabs.open(tabOptions);
});



proxy.registerEndpoint('tabs:query', function(data, callback) {
    var { query } = data;

    var urls = [];
    if('url' in query) {
        switch(Object.prototype.toString.apply(query.url)) {
            case '[object String]':
                urls.push(query.url);
                break;

            case '[object Array]':
                urls = query.url;
                break;
        }
    }

    var active = null;
    if('active' in query)
        active = !!query.active;

    var activeWindow = null;
    if('lastFocusedWindow' in query)
        activeWindow = !!query.lastFocusedWindow;

    var matches = [].filter.call(tabs, function(tab) {
        if(urls.length && urls.indexOf(tab.url) === -1)
            return false;

        if(active !== null && active !== (tab.id === tab.window.tabs.activeTab.id))
            return false;

        if(activeWindow !== null && activeWindow !== (tab.window === windows.activeWindow))
            return false;

        return true;
    });

    matches = matches.map(function(tab) {
        return {
            id:    tab.id,
            title: tab.title,
            url:   tab.url
        };
    });

    callback({ error: false, tabs: matches });
});



proxy.registerEndpoint('tabs:remove', function(data, callback) {
    var { id } = data;
    var matchedTab = null;

    for(let tab of tabs) {
        if(tab.id === id) {
            matchedTab = tab;
            break;
        }
    }

    if(matchedTab) {
        matchedTab.close(function() {
            callback({ error: false });
        });
    }
    else
        callback({ error: false });
});



proxy.registerEndpoint('tabs:update', function(data, callback) {
    var { id, properties } = data;

    for(let tab of tabs) {
        if(tab.id === id) {
            if('url' in properties && typeof properties.url === 'string')
                tab.url = properties.url;

            if('active' in properties && properties.active)
                tab.activate();

            callback({ error: false, tab: {
                id:    tab.id,
                title: tab.title,
                url:   tab.url
            }});
            return;
        }
    }

    callback({ error: false, tab: null });
});
