/*jshint moz:true*/

var windows = require('sdk/windows').browserWindows;
var self = require('sdk/self');
var { Cc, Ci, Cu } = require('chrome');
var { Class } = require('sdk/core/heritage');
var { Unknown } = require('sdk/platform/xpcom');
var { setTimeout, setInterval, clearTimeout, clearInterval } = require('sdk/timers');

var proxy = require('./../page-proxy');

var watcher = Cc["@mozilla.org/embedcomp/window-watcher;1"].getService(Ci.nsIWindowWatcher);

//tracking current open windows
var open = {};

//get id from observer message subject
var getWindowBySubject = function(subject) {
    for(var id in open) {
        if(open[id] === subject) {
            return parseInt(id, 10);
        }
    }
    return null;
};

//get window from id
var getWindowById = function(id) {
    return open[id];
};

var windowStatus= {};
var WindowObserver = Class({
    extends: Unknown,
    interfaces: [ 'nsIObserver' ],
    topic: '*',
    observe: function(subject, topic, data) {
        var win = getWindowBySubject(subject);
        switch(topic) {
            case 'domwindowopened':
                var monitorInterval = setInterval(function() {
                    var win = getWindowBySubject(subject);
                    if(subject.document.body) {
                        var className = subject.document.body.className;
                        if(win) {
                            if(!windowStatus[win]) {
                                windowStatus[win] = {};
                                windowStatus[win].interval = monitorInterval;
                            }
                            if(className !== windowStatus[win].className) {
                                windowStatus[win].className = className;
                                proxy.broadcast('windows:on_class_changed', { id: parseInt(win, 10), className: className });
                            }
                        }
                    }
                }, 1000);
                break;
            case 'domwindowclosed':
                if(win) {
                    clearInterval(windowStatus[win].interval);
                    delete open[win];
                    delete windowStatus[win];
                    proxy.broadcast('windows:on_remove', { windowId: win });
                }
                break;
            default:
                //there should be only open/close messages on this observer
                //console.log(subject, topic, data);
                break;
        }
    }
});

watcher.registerNotification(new WindowObserver());

proxy.registerEndpoint('windows:create', function(data, callback) {
    var win = watcher.openWindow(null, data.createData.url, '', data.createData, null);
    var id = win.mozAnimationStartTime;
    open[id] = win;
    callback({ error: false, window: { id, url: data.createData.url, tabs: [] } });

    // @BUG@
    proxy.broadcast('windows:on_create', { event: 'created', windowId: id });
});


