/*jshint moz:true*/

let { getFavicon } = require('sdk/places/favicon');



exports.getFavicon = function(url, callback) {
    if(typeof url !== 'string') {
        callback(null);
        return;
    }

    getFavicon(url).then(
        // Success
        function(favicon) {
            callback(favicon.indexOf('made-up-favicon') === -1 ? favicon : null);
        },
        // Failure
        function(error) {
            callback(null);
        }
    );
};
