/*jshint moz:true*/

//var newtab  = require('./../newtab');
var proxy   = require('./../page-proxy');
var runtime = require('./../runtime');



//proxy.registerEndpoint('runtime:get_newtab_url', function(data, callback) {
//    callback({ error: false, url: newtab.URL });
//});



proxy.registerEndpoint('runtime:reload', function(data) {
    // Does nothing in firefox
});



proxy.registerEndpoint('runtime:send_message', function(data) {
    proxy.broadcast('runtime:on_message', data);
});

proxy.registerEndpoint('runtime:send_message_response', function(data) {
    proxy.broadcast('runtime:on_message_response', data);
});



runtime.events.load(function(info) {
    proxy.broadcast('runtime:on_load', info);
});

runtime.events.unload(function(info) {
    proxy.broadcast('runtime:on_unload', info);
});

