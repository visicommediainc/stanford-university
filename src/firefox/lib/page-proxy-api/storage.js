/*jshint moz:true*/

var self = require('sdk/self');
var ss = require('sdk/simple-storage');

var EH = require(self.data.url('js/event-handler'));
var proxy = require('./../page-proxy');



exports.clear = function(callback) {
    console.log("storage.clear.");
    //console.log("storage.clear." + JSON.stringify(ss.storage));
    var changes = {}, hasChanges = false;

    for (let prop in ss.storage) {
      console.log("obj." + prop + " = " + ss.storage[prop]);
      delete ss.storage[prop];
      hasChanges = true;
      changes[prop] = { oldValue: ss.storage[prop] };
   }
   console.log("storage.clear." + JSON.stringify(ss.storage));
   EH.safe(callback)(false);


    EH.safe(callback)(false);

    if(hasChanges)
        exports.onChange.fire(changes);
  
};

exports.get = function(anyKeys, callback) {
    var keys = [];
    var items = {};

    switch(Object.prototype.toString.apply(anyKeys)) {
        case '[object Null]':
            EH.safe(callback)(false, ss.storage);
            return;

        case '[object String]':
            keys.push(anyKeys);
            break;

        case '[object Array]':
            keys = anyKeys;
            break;

        case '[object Object]':
            for(let key in anyKeys)
                keys.push(key);
            break;
    }

    for(let i = 0; i < keys.length; ++i)
        if(ss.storage.hasOwnProperty(keys[i]))
            items[keys[i]] = ss.storage[keys[i]];

    EH.safe(callback)(false, items);
};

exports.remove = function(anyKeys, callback) {
    var changes = {}, hasChanges = false;
    var keys = [];

    switch(Object.prototype.toString.apply(anyKeys)) {
        case '[object String]':
            keys.push(anyKeys);
            break;

        case '[object Array]':
            keys = anyKeys;
            break;

        case '[object Object]':
            for(let key in anyKeys)
                keys.push(key);
            break;
    }

    for(let i = 0; i < keys.length; ++i) {
        if(ss.storage.hasOwnProperty(keys[i])) {
            hasChanges = true;
            changes[keys[i]] = { oldValue: ss.storage[keys[i]] };

            delete ss.storage[keys[i]];
        }
    }

    EH.safe(callback)(false);

    if(hasChanges)
        exports.onChange.fire(changes);
};

exports.set = function(items, callback) {
    var changes = {}, hasChanges = false;

    for(let i in items) {
        if(ss.storage[i] === items[i] && (typeof items[i] !== 'object' || items[i] === null))
            continue;

        hasChanges = true;
        changes[i] = {
            oldValue: ss.storage[i],
            newValue: items[i]
        };

        ss.storage[i] = items[i];
    }

    EH.safe(callback)(false);

    if(hasChanges)
        exports.onChange.fire(changes);
};

exports.onChange = EH();



proxy.registerEndpoint('storage:clear', function(data, callback) {
    exports.clear(function(error) {
        callback({ error });
    });
});

proxy.registerEndpoint('storage:get', function(data, callback) {
    exports.get(data.keys, function(error, items) {
        callback({ error, items });
    });
});

proxy.registerEndpoint('storage:remove', function(data, callback) {
    exports.set(data.keys, function(error) {
        callback({ error });
    });
});

proxy.registerEndpoint('storage:set', function(data, callback) {
    exports.set(data.items, function(error) {
        callback({ error });
    });
});

exports.onChange(function(changes) {
    proxy.broadcast('storage:on_change', changes);
});
