    var { ActionButton } = require("sdk/ui/button/action");
    var panels  = require("sdk/panel");
    var system = require("sdk/system");
    var { setTimeout } = require("sdk/timers");
    var { setInterval } = require("sdk/timers");
    var tabs    = require("sdk/tabs");
    var data = require("sdk/self").data;
    var Panel   = require('./settings').Panel;
    var panelURL = data.url("html/popup.html");
    var pjson = require('../package.json');

    function clicked(state) 
    {
        panel.show({
            height: Panel.height ,
            width : Panel.width ,
            contentURL: panelURL ,
            position: button
        });
    }
 
    function changed(state) 
    {
    } 
    
    var button = ActionButton({
      id : "stanford-button",
      label: "Stanford GSB Library",
      icon: {
        '16': './icons/16.png',
        '18': './icons/18.png',
        '32': './icons/32.png',
        '36': './icons/36.png',
        '64': './icons/64.png'
      },
      onChange: changed,
      onClick: clicked
    });
    
    loadWebpage = function(url) {
        //console.log("utils.loadWebpage");
        tabs.open(url);
    };
    
    var panel = panels.Panel({ 
        height: Panel.height ,
        width : Panel.width ,
        contentURL: panelURL ,
        contentScriptFile: data.url("js/content-script.js")
    });
    
    panel.on("show", function() {
        panel.port.emit("show", {version: pjson.version});
        //console.log("version:" + pjson.version);
    });
   
    panel.port.on("*", function(request) {
        //console.log("main.popup.port.on." + JSON.stringify(request));
        switch(request.message)
        {
            case "navigate":
                loadWebpage(request.url);
            break;
        }
    });

    exports.button  = button;
    exports.panel   = panel;
